mod handler;

use axum::routing::get;
use std::collections::HashMap;

#[tokio::main]
async fn main() {
    println!("starting the web server on 8080");

    let mut paths_to_urls = HashMap::new();
    paths_to_urls.insert(
        "/urlshort-godoc".to_string(),
        "https://crates.io/crates/axum".to_string(),
    );
    paths_to_urls.insert(
        "/yaml-godoc".to_string(),
        "https://github.com/tokio-rs/axum".to_string(),
    );
    let app = handler::map_string_handler(paths_to_urls);

    let yaml_url = "
    - path: /urlshort
      url: https://crates.io/crates/yaml-rust
    - path: /urlshort-final
      url: https://github.com/chyh1990/yaml-rust
       ";

    let app = handler::map_yaml_handler(yaml_url, app);

    // default route if no other route matches (can be for custom 404 handler)
    let app = app.fallback(get(|| async { "Invalid route was entered!" }));

    axum::Server::bind(&"127.0.0.1:8080".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}
